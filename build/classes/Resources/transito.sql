-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 20-05-2016 a las 08:40:30
-- Versión del servidor: 5.6.30-0ubuntu0.15.10.1
-- Versión de PHP: 5.6.11-1ubuntu3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `transito`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `casaautomovil`
--

CREATE TABLE IF NOT EXISTS `casaautomovil` (
  `idCasaAutomovil` int(11) NOT NULL,
  `NombreCasaAutomovil` varchar(45) NOT NULL,
  `Direccion` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `casaautomovil`
--

INSERT INTO `casaautomovil` (`idCasaAutomovil`, `NombreCasaAutomovil`, `Direccion`) VALUES
(1, 'Compañia Colombiana Automotriz', 'Cl. 13 #38-54, Bogotá'),
(2, 'General Motors Colmotores', 'Av. Boyaca Cll 56A Sur No. 33-53, Bogotá D.C.'),
(3, 'Sofasa', 'Carrera 49 # 39 Sur - 100 Envigado, Antioquia'),
(4, 'Hino Motors Manufacturing S.A', 'Av El Dorado 78 20 , COTA, Bogota'),
(5, 'Navitrans', 'Cr15 21-02, Bucaramanga'),
(6, 'Carrocerías Non Plus Ultra', 'Cr49 201-50, Bogotá'),
(7, 'Compañia de Autoensamble Nissan', ' Cl 13 N° 50-69, Bogotá'),
(8, 'Praco Didacol', 'Av El Dorado N° 78-20, Bogotá');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE IF NOT EXISTS `ciudad` (
  `idCiudad` int(11) NOT NULL,
  `NombreCiudad` varchar(100) NOT NULL,
  `idDepartamento` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1121 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`idCiudad`, `NombreCiudad`, `idDepartamento`) VALUES
(1, 'MEDELLIN', 2),
(2, 'ABEJORRAL', 2),
(3, 'ABRIAQUI', 2),
(4, 'ALEJANDRIA', 2),
(5, 'AMAGA', 2),
(6, 'AMALFI', 2),
(7, 'ANDES', 2),
(8, 'ANGELOPOLIS', 2),
(9, 'ANGOSTURA', 2),
(10, 'ANORI', 2),
(11, 'SANTAFE DE ANTIOQUIA', 2),
(12, 'ANZA', 2),
(13, 'APARTADO', 2),
(14, 'ARBOLETES', 2),
(15, 'ARGELIA', 2),
(16, 'ARMENIA', 2),
(17, 'BARBOSA', 2),
(18, 'BELMIRA', 2),
(19, 'BELLO', 2),
(20, 'BETANIA', 2),
(21, 'BETULIA', 2),
(22, 'CIUDAD BOLIVAR', 2),
(23, 'BRICEÑO', 2),
(24, 'BURITICA', 2),
(25, 'CACERES', 2),
(26, 'CAICEDO', 2),
(27, 'CALDAS', 2),
(28, 'CAMPAMENTO', 2),
(29, 'CAÑASGORDAS', 2),
(30, 'CARACOLI', 2),
(31, 'CARAMANTA', 2),
(32, 'CAREPA', 2),
(33, 'EL CARMEN DE VIBORAL', 2),
(34, 'CAROLINA', 2),
(35, 'CAUCASIA', 2),
(36, 'CHIGORODO', 2),
(37, 'CISNEROS', 2),
(38, 'COCORNA', 2),
(39, 'CONCEPCION', 2),
(40, 'CONCORDIA', 2),
(41, 'COPACABANA', 2),
(42, 'DABEIBA', 2),
(43, 'DON MATIAS', 2),
(44, 'EBEJICO', 2),
(45, 'EL BAGRE', 2),
(46, 'ENTRERRIOS', 2),
(47, 'ENVIGADO', 2),
(48, 'FREDONIA', 2),
(49, 'FRONTINO', 2),
(50, 'GIRALDO', 2),
(51, 'GIRARDOTA', 2),
(52, 'GOMEZ PLATA', 2),
(53, 'GRANADA', 2),
(54, 'GUADALUPE', 2),
(55, 'GUARNE', 2),
(56, 'GUATAPE', 2),
(57, 'HELICONIA', 2),
(58, 'HISPANIA', 2),
(59, 'ITAGUI', 2),
(60, 'ITUANGO', 2),
(61, 'JARDIN', 2),
(62, 'JERICO', 2),
(63, 'LA CEJA', 2),
(64, 'LA ESTRELLA', 2),
(65, 'LA PINTADA', 2),
(66, 'LA UNION', 2),
(67, 'LIBORINA', 2),
(68, 'MACEO', 2),
(69, 'MARINILLA', 2),
(70, 'MONTEBELLO', 2),
(71, 'MURINDO', 2),
(72, 'MUTATA', 2),
(73, 'NARIÑO', 2),
(74, 'NECOCLI', 2),
(75, 'NECHI', 2),
(76, 'OLAYA', 2),
(77, 'PEÐOL', 2),
(78, 'PEQUE', 2),
(79, 'PUEBLORRICO', 2),
(80, 'PUERTO BERRIO', 2),
(81, 'PUERTO NARE', 2),
(82, 'PUERTO TRIUNFO', 2),
(83, 'REMEDIOS', 2),
(84, 'RETIRO', 2),
(85, 'RIONEGRO', 2),
(86, 'SABANALARGA', 2),
(87, 'SABANETA', 2),
(88, 'SALGAR', 2),
(89, 'SAN ANDRES DE CUERQUIA', 2),
(90, 'SAN CARLOS', 2),
(91, 'SAN FRANCISCO', 2),
(92, 'SAN JERONIMO', 2),
(93, 'SAN JOSE DE LA MONTAÑA', 2),
(94, 'SAN JUAN DE URABA', 2),
(95, 'SAN LUIS', 2),
(96, 'SAN PEDRO', 2),
(97, 'SAN PEDRO DE URABA', 2),
(98, 'SAN RAFAEL', 2),
(99, 'SAN ROQUE', 2),
(100, 'SAN VICENTE', 2),
(101, 'SANTA BARBARA', 2),
(102, 'SANTA ROSA DE OSOS', 2),
(103, 'SANTO DOMINGO', 2),
(104, 'EL SANTUARIO', 2),
(105, 'SEGOVIA', 2),
(106, 'SONSON', 2),
(107, 'SOPETRAN', 2),
(108, 'TAMESIS', 2),
(109, 'TARAZA', 2),
(110, 'TARSO', 2),
(111, 'TITIRIBI', 2),
(112, 'TOLEDO', 2),
(113, 'TURBO', 2),
(114, 'URAMITA', 2),
(115, 'URRAO', 2),
(116, 'VALDIVIA', 2),
(117, 'VALPARAISO', 2),
(118, 'VEGACHI', 2),
(119, 'VENECIA', 2),
(120, 'VIGIA DEL FUERTE', 2),
(121, 'YALI', 2),
(122, 'YARUMAL', 2),
(123, 'YOLOMBO', 2),
(124, 'YONDO', 2),
(125, 'ZARAGOZA', 2),
(126, 'BARRANQUILLA', 4),
(127, 'BARANOA', 4),
(128, 'CAMPO DE LA CRUZ', 4),
(129, 'CANDELARIA', 4),
(130, 'GALAPA', 4),
(131, 'JUAN DE ACOSTA', 4),
(132, 'LURUACO', 4),
(133, 'MALAMBO', 4),
(134, 'MANATI', 4),
(135, 'PALMAR DE VARELA', 4),
(136, 'PIOJO', 4),
(137, 'POLONUEVO', 4),
(138, 'PONEDERA', 4),
(139, 'PUERTO COLOMBIA', 4),
(140, 'REPELON', 4),
(141, 'SABANAGRANDE', 4),
(142, 'SABANALARGA', 4),
(143, 'SANTA LUCIA', 4),
(144, 'SANTO TOMAS', 4),
(145, 'SOLEDAD', 4),
(146, 'SUAN', 4),
(147, 'TUBARA', 4),
(148, 'USIACURI', 4),
(149, 'BOGOTA, D.C.', 14),
(150, 'CARTAGENA', 5),
(151, 'ACHI', 5),
(152, 'ALTOS DEL ROSARIO', 5),
(153, 'ARENAL', 5),
(154, 'ARJONA', 5),
(155, 'ARROYOHONDO', 5),
(156, 'BARRANCO DE LOBA', 5),
(157, 'CALAMAR', 5),
(158, 'CANTAGALLO', 5),
(159, 'CICUCO', 5),
(160, 'CORDOBA', 5),
(161, 'CLEMENCIA', 5),
(162, 'EL CARMEN DE BOLIVAR', 5),
(163, 'EL GUAMO', 5),
(164, 'EL PEÑON', 5),
(165, 'HATILLO DE LOBA', 5),
(166, 'MAGANGUE', 5),
(167, 'MAHATES', 5),
(168, 'MARGARITA', 5),
(169, 'MARIA LA BAJA', 5),
(170, 'MONTECRISTO', 5),
(171, 'MOMPOS', 5),
(172, 'NOROSI', 5),
(173, 'MORALES', 5),
(174, 'PINILLOS', 5),
(175, 'REGIDOR', 5),
(176, 'RIO VIEJO', 5),
(177, 'SAN CRISTOBAL', 5),
(178, 'SAN ESTANISLAO', 5),
(179, 'SAN FERNANDO', 5),
(180, 'SAN JACINTO', 5),
(181, 'SAN JACINTO DEL CAUCA', 5),
(182, 'SAN JUAN NEPOMUCENO', 5),
(183, 'SAN MARTIN DE LOBA', 5),
(184, 'SAN PABLO', 5),
(185, 'SANTA CATALINA', 5),
(186, 'SANTA ROSA', 5),
(187, 'SANTA ROSA DEL SUR', 5),
(188, 'SIMITI', 5),
(189, 'SOPLAVIENTO', 5),
(190, 'TALAIGUA NUEVO', 5),
(191, 'TIQUISIO', 5),
(192, 'TURBACO', 5),
(193, 'TURBANA', 5),
(194, 'VILLANUEVA', 5),
(195, 'ZAMBRANO', 5),
(196, 'TUNJA', 6),
(197, 'ALMEIDA', 6),
(198, 'AQUITANIA', 6),
(199, 'ARCABUCO', 6),
(200, 'BELEN', 6),
(201, 'BERBEO', 6),
(202, 'BETEITIVA', 6),
(203, 'BOAVITA', 6),
(204, 'BOYACA', 6),
(205, 'BRICEÑO', 6),
(206, 'BUENAVISTA', 6),
(207, 'BUSBANZA', 6),
(208, 'CALDAS', 6),
(209, 'CAMPOHERMOSO', 6),
(210, 'CERINZA', 6),
(211, 'CHINAVITA', 6),
(212, 'CHIQUINQUIRA', 6),
(213, 'CHISCAS', 6),
(214, 'CHITA', 6),
(215, 'CHITARAQUE', 6),
(216, 'CHIVATA', 6),
(217, 'CIENEGA', 6),
(218, 'COMBITA', 6),
(219, 'COPER', 6),
(220, 'CORRALES', 6),
(221, 'COVARACHIA', 6),
(222, 'CUBARA', 6),
(223, 'CUCAITA', 6),
(224, 'CUITIVA', 6),
(225, 'CHIQUIZA', 6),
(226, 'CHIVOR', 6),
(227, 'DUITAMA', 6),
(228, 'EL COCUY', 6),
(229, 'EL ESPINO', 6),
(230, 'FIRAVITOBA', 6),
(231, 'FLORESTA', 6),
(232, 'GACHANTIVA', 6),
(233, 'GAMEZA', 6),
(234, 'GARAGOA', 6),
(235, 'GUACAMAYAS', 6),
(236, 'GUATEQUE', 6),
(237, 'GUAYATA', 6),
(238, 'GsICAN', 6),
(239, 'IZA', 6),
(240, 'JENESANO', 6),
(241, 'JERICO', 6),
(242, 'LABRANZAGRANDE', 6),
(243, 'LA CAPILLA', 6),
(244, 'LA VICTORIA', 6),
(245, 'LA UVITA', 6),
(246, 'VILLA DE LEYVA', 6),
(247, 'MACANAL', 6),
(248, 'MARIPI', 6),
(249, 'MIRAFLORES', 6),
(250, 'MONGUA', 6),
(251, 'MONGUI', 6),
(252, 'MONIQUIRA', 6),
(253, 'MOTAVITA', 6),
(254, 'MUZO', 6),
(255, 'NOBSA', 6),
(256, 'NUEVO COLON', 6),
(257, 'OICATA', 6),
(258, 'OTANCHE', 6),
(259, 'PACHAVITA', 6),
(260, 'PAEZ', 6),
(261, 'PAIPA', 6),
(262, 'PAJARITO', 6),
(263, 'PANQUEBA', 6),
(264, 'PAUNA', 6),
(265, 'PAYA', 6),
(266, 'PAZ DE RIO', 6),
(267, 'PESCA', 6),
(268, 'PISBA', 6),
(269, 'PUERTO BOYACA', 6),
(270, 'QUIPAMA', 6),
(271, 'RAMIRIQUI', 6),
(272, 'RAQUIRA', 6),
(273, 'RONDON', 6),
(274, 'SABOYA', 6),
(275, 'SACHICA', 6),
(276, 'SAMACA', 6),
(277, 'SAN EDUARDO', 6),
(278, 'SAN JOSE DE PARE', 6),
(279, 'SAN LUIS DE GACENO', 6),
(280, 'SAN MATEO', 6),
(281, 'SAN MIGUEL DE SEMA', 6),
(282, 'SAN PABLO DE BORBUR', 6),
(283, 'SANTANA', 6),
(284, 'SANTA MARIA', 6),
(285, 'SANTA ROSA DE VITERBO', 6),
(286, 'SANTA SOFIA', 6),
(287, 'SATIVANORTE', 6),
(288, 'SATIVASUR', 6),
(289, 'SIACHOQUE', 6),
(290, 'SOATA', 6),
(291, 'SOCOTA', 6),
(292, 'SOCHA', 6),
(293, 'SOGAMOSO', 6),
(294, 'SOMONDOCO', 6),
(295, 'SORA', 6),
(296, 'SOTAQUIRA', 6),
(297, 'SORACA', 6),
(298, 'SUSACON', 6),
(299, 'SUTAMARCHAN', 6),
(300, 'SUTATENZA', 6),
(301, 'TASCO', 6),
(302, 'TENZA', 6),
(303, 'TIBANA', 6),
(304, 'TIBASOSA', 6),
(305, 'TINJACA', 6),
(306, 'TIPACOQUE', 6),
(307, 'TOCA', 6),
(308, 'TOGsI', 6),
(309, 'TOPAGA', 6),
(310, 'TOTA', 6),
(311, 'TUNUNGUA', 6),
(312, 'TURMEQUE', 6),
(313, 'TUTA', 6),
(314, 'TUTAZA', 6),
(315, 'UMBITA', 6),
(316, 'VENTAQUEMADA', 6),
(317, 'VIRACACHA', 6),
(318, 'ZETAQUIRA', 6),
(319, 'MANIZALES', 7),
(320, 'AGUADAS', 7),
(321, 'ANSERMA', 7),
(322, 'ARANZAZU', 7),
(323, 'BELALCAZAR', 7),
(324, 'CHINCHINA', 7),
(325, 'FILADELFIA', 7),
(326, 'LA DORADA', 7),
(327, 'LA MERCED', 7),
(328, 'MANZANARES', 7),
(329, 'MARMATO', 7),
(330, 'MARQUETALIA', 7),
(331, 'MARULANDA', 7),
(332, 'NEIRA', 7),
(333, 'NORCASIA', 7),
(334, 'PACORA', 7),
(335, 'PALESTINA', 7),
(336, 'PENSILVANIA', 7),
(337, 'RIOSUCIO', 7),
(338, 'RISARALDA', 7),
(339, 'SALAMINA', 7),
(340, 'SAMANA', 7),
(341, 'SAN JOSE', 7),
(342, 'SUPIA', 7),
(343, 'VICTORIA', 7),
(344, 'VILLAMARIA', 7),
(345, 'VITERBO', 7),
(346, 'FLORENCIA', 8),
(347, 'ALBANIA', 8),
(348, 'BELEN DE LOS ANDAQUIES', 8),
(349, 'CARTAGENA DEL CHAIRA', 8),
(350, 'CURILLO', 8),
(351, 'EL DONCELLO', 8),
(352, 'EL PAUJIL', 8),
(353, 'LA MONTAÑITA', 8),
(354, 'MILAN', 8),
(355, 'MORELIA', 8),
(356, 'PUERTO RICO', 8),
(357, 'SAN JOSE DEL FRAGUA', 8),
(358, 'SAN VICENTE DEL CAGUAN', 8),
(359, 'SOLANO', 8),
(360, 'SOLITA', 8),
(361, 'VALPARAISO', 8),
(362, 'POPAYAN', 10),
(363, 'ALMAGUER', 10),
(364, 'ARGELIA', 10),
(365, 'BALBOA', 10),
(366, 'BOLIVAR', 10),
(367, 'BUENOS AIRES', 10),
(368, 'CAJIBIO', 10),
(369, 'CALDONO', 10),
(370, 'CALOTO', 10),
(371, 'CORINTO', 10),
(372, 'EL TAMBO', 10),
(373, 'FLORENCIA', 10),
(374, 'GUACHENE', 10),
(375, 'GUAPI', 10),
(376, 'INZA', 10),
(377, 'JAMBALO', 10),
(378, 'LA SIERRA', 10),
(379, 'LA VEGA', 10),
(380, 'LOPEZ', 10),
(381, 'MERCADERES', 10),
(382, 'MIRANDA', 10),
(383, 'MORALES', 10),
(384, 'PADILLA', 10),
(385, 'PAEZ', 10),
(386, 'PATIA', 10),
(387, 'PIAMONTE', 10),
(388, 'PIENDAMO', 10),
(389, 'PUERTO TEJADA', 10),
(390, 'PURACE', 10),
(391, 'ROSAS', 10),
(392, 'SAN SEBASTIAN', 10),
(393, 'SANTANDER DE QUILICHAO', 10),
(394, 'SANTA ROSA', 10),
(395, 'SILVIA', 10),
(396, 'SOTARA', 10),
(397, 'SUAREZ', 10),
(398, 'SUCRE', 10),
(399, 'TIMBIO', 10),
(400, 'TIMBIQUI', 10),
(401, 'TORIBIO', 10),
(402, 'TOTORO', 10),
(403, 'VILLA RICA', 10),
(404, 'VALLEDUPAR', 11),
(405, 'AGUACHICA', 11),
(406, 'AGUSTIN CODAZZI', 11),
(407, 'ASTREA', 11),
(408, 'BECERRIL', 11),
(409, 'BOSCONIA', 11),
(410, 'CHIMICHAGUA', 11),
(411, 'CHIRIGUANA', 11),
(412, 'CURUMANI', 11),
(413, 'EL COPEY', 11),
(414, 'EL PASO', 11),
(415, 'GAMARRA', 11),
(416, 'GONZALEZ', 11),
(417, 'LA GLORIA', 11),
(418, 'LA JAGUA DE IBIRICO', 11),
(419, 'MANAURE', 11),
(420, 'PAILITAS', 11),
(421, 'PELAYA', 11),
(422, 'PUEBLO BELLO', 11),
(423, 'RIO DE ORO', 11),
(424, 'LA PAZ', 11),
(425, 'SAN ALBERTO', 11),
(426, 'SAN DIEGO', 11),
(427, 'SAN MARTIN', 11),
(428, 'TAMALAMEQUE', 11),
(429, 'MONTERIA', 13),
(430, 'AYAPEL', 13),
(431, 'BUENAVISTA', 13),
(432, 'CANALETE', 13),
(433, 'CERETE', 13),
(434, 'CHIMA', 13),
(435, 'CHINU', 13),
(436, 'CIENAGA DE ORO', 13),
(437, 'COTORRA', 13),
(438, 'LA APARTADA', 13),
(439, 'LORICA', 13),
(440, 'LOS CORDOBAS', 13),
(441, 'MOMIL', 13),
(442, 'MONTELIBANO', 13),
(443, 'MOÑITOS', 13),
(444, 'PLANETA RICA', 13),
(445, 'PUEBLO NUEVO', 13),
(446, 'PUERTO ESCONDIDO', 13),
(447, 'PUERTO LIBERTADOR', 13),
(448, 'PURISIMA', 13),
(449, 'SAHAGUN', 13),
(450, 'SAN ANDRES SOTAVENTO', 13),
(451, 'SAN ANTERO', 13),
(452, 'SAN BERNARDO DEL VIENTO', 13),
(453, 'SAN CARLOS', 13),
(454, 'SAN PELAYO', 13),
(455, 'TIERRALTA', 13),
(456, 'VALENCIA', 13),
(457, 'AGUA DE DIOS', 14),
(458, 'ALBAN', 14),
(459, 'ANAPOIMA', 14),
(460, 'ANOLAIMA', 14),
(461, 'ARBELAEZ', 14),
(462, 'BELTRAN', 14),
(463, 'BITUIMA', 14),
(464, 'BOJACA', 14),
(465, 'CABRERA', 14),
(466, 'CACHIPAY', 14),
(467, 'CAJICA', 14),
(468, 'CAPARRAPI', 14),
(469, 'CAQUEZA', 14),
(470, 'CARMEN DE CARUPA', 14),
(471, 'CHAGUANI', 14),
(472, 'CHIA', 14),
(473, 'CHIPAQUE', 14),
(474, 'CHOACHI', 14),
(475, 'CHOCONTA', 14),
(476, 'COGUA', 14),
(477, 'COTA', 14),
(478, 'CUCUNUBA', 14),
(479, 'EL COLEGIO', 14),
(480, 'EL PEÑON', 14),
(481, 'EL ROSAL', 14),
(482, 'FACATATIVA', 14),
(483, 'FOMEQUE', 14),
(484, 'FOSCA', 14),
(485, 'FUNZA', 14),
(486, 'FUQUENE', 14),
(487, 'FUSAGASUGA', 14),
(488, 'GACHALA', 14),
(489, 'GACHANCIPA', 14),
(490, 'GACHETA', 14),
(491, 'GAMA', 14),
(492, 'GIRARDOT', 14),
(493, 'GRANADA', 14),
(494, 'GUACHETA', 14),
(495, 'GUADUAS', 14),
(496, 'GUASCA', 14),
(497, 'GUATAQUI', 14),
(498, 'GUATAVITA', 14),
(499, 'GUAYABAL DE SIQUIMA', 14),
(500, 'GUAYABETAL', 14),
(501, 'GUTIERREZ', 14),
(502, 'JERUSALEN', 14),
(503, 'JUNIN', 14),
(504, 'LA CALERA', 14),
(505, 'LA MESA', 14),
(506, 'LA PALMA', 14),
(507, 'LA PEÑA', 14),
(508, 'LA VEGA', 14),
(509, 'LENGUAZAQUE', 14),
(510, 'MACHETA', 14),
(511, 'MADRID', 14),
(512, 'MANTA', 14),
(513, 'MEDINA', 14),
(514, 'MOSQUERA', 14),
(515, 'NARIÑO', 14),
(516, 'NEMOCON', 14),
(517, 'NILO', 14),
(518, 'NIMAIMA', 14),
(519, 'NOCAIMA', 14),
(520, 'VENECIA', 14),
(521, 'PACHO', 14),
(522, 'PAIME', 14),
(523, 'PANDI', 14),
(524, 'PARATEBUENO', 14),
(525, 'PASCA', 14),
(526, 'PUERTO SALGAR', 14),
(527, 'PULI', 14),
(528, 'QUEBRADANEGRA', 14),
(529, 'QUETAME', 14),
(530, 'QUIPILE', 14),
(531, 'APULO', 14),
(532, 'RICAURTE', 14),
(533, 'SAN ANTONIO DEL TEQUENDAMA', 14),
(534, 'SAN BERNARDO', 14),
(535, 'SAN CAYETANO', 14),
(536, 'SAN FRANCISCO', 14),
(537, 'SAN JUAN DE RIO SECO', 14),
(538, 'SASAIMA', 14),
(539, 'SESQUILE', 14),
(540, 'SIBATE', 14),
(541, 'SILVANIA', 14),
(542, 'SIMIJACA', 14),
(543, 'SOACHA', 14),
(544, 'SOPO', 14),
(545, 'SUBACHOQUE', 14),
(546, 'SUESCA', 14),
(547, 'SUPATA', 14),
(548, 'SUSA', 14),
(549, 'SUTATAUSA', 14),
(550, 'TABIO', 14),
(551, 'TAUSA', 14),
(552, 'TENA', 14),
(553, 'TENJO', 14),
(554, 'TIBACUY', 14),
(555, 'TIBIRITA', 14),
(556, 'TOCAIMA', 14),
(557, 'TOCANCIPA', 14),
(558, 'TOPAIPI', 14),
(559, 'UBALA', 14),
(560, 'UBAQUE', 14),
(561, 'VILLA DE SAN DIEGO DE UBATE', 14),
(562, 'UNE', 14),
(563, 'UTICA', 14),
(564, 'VERGARA', 14),
(565, 'VIANI', 14),
(566, 'VILLAGOMEZ', 14),
(567, 'VILLAPINZON', 14),
(568, 'VILLETA', 14),
(569, 'VIOTA', 14),
(570, 'YACOPI', 14),
(571, 'ZIPACON', 14),
(572, 'ZIPAQUIRA', 14),
(573, 'QUIBDO', 12),
(574, 'ACANDI', 12),
(575, 'ALTO BAUDO', 12),
(576, 'ATRATO', 12),
(577, 'BAGADO', 12),
(578, 'BAHIA SOLANO', 12),
(579, 'BAJO BAUDO', 12),
(580, 'BOJAYA', 12),
(581, 'EL CANTON DEL SAN PABLO', 12),
(582, 'CARMEN DEL DARIEN', 12),
(583, 'CERTEGUI', 12),
(584, 'CONDOTO', 12),
(585, 'EL CARMEN DE ATRATO', 12),
(586, 'EL LITORAL DEL SAN JUAN', 12),
(587, 'ISTMINA', 12),
(588, 'JURADO', 12),
(589, 'LLORO', 12),
(590, 'MEDIO ATRATO', 12),
(591, 'MEDIO BAUDO', 12),
(592, 'MEDIO SAN JUAN', 12),
(593, 'NOVITA', 12),
(594, 'NUQUI', 12),
(595, 'RIO IRO', 12),
(596, 'RIO QUITO', 12),
(597, 'RIOSUCIO', 12),
(598, 'SAN JOSE DEL PALMAR', 12),
(599, 'SIPI', 12),
(600, 'TADO', 12),
(601, 'UNGUIA', 12),
(602, 'UNION PANAMERICANA', 12),
(603, 'NEIVA', 12),
(604, 'ACEVEDO', 12),
(605, 'AGRADO', 12),
(606, 'AIPE', 12),
(607, 'ALGECIRAS', 12),
(608, 'ALTAMIRA', 12),
(609, 'BARAYA', 12),
(610, 'CAMPOALEGRE', 12),
(611, 'COLOMBIA', 12),
(612, 'ELIAS', 12),
(613, 'GARZON', 12),
(614, 'GIGANTE', 12),
(615, 'GUADALUPE', 12),
(616, 'HOBO', 12),
(617, 'IQUIRA', 12),
(618, 'ISNOS', 12),
(619, 'LA ARGENTINA', 12),
(620, 'LA PLATA', 12),
(621, 'NATAGA', 12),
(622, 'OPORAPA', 12),
(623, 'PAICOL', 12),
(624, 'PALERMO', 12),
(625, 'PALESTINA', 12),
(626, 'PITAL', 12),
(627, 'PITALITO', 12),
(628, 'RIVERA', 12),
(629, 'SALADOBLANCO', 12),
(630, 'SAN AGUSTIN', 12),
(631, 'SANTA MARIA', 12),
(632, 'SUAZA', 12),
(633, 'TARQUI', 12),
(634, 'TESALIA', 12),
(635, 'TELLO', 12),
(636, 'TERUEL', 12),
(637, 'TIMANA', 12),
(638, 'VILLAVIEJA', 12),
(639, 'YAGUARA', 12),
(640, 'RIOHACHA', 18),
(641, 'ALBANIA', 18),
(642, 'BARRANCAS', 18),
(643, 'DIBULLA', 18),
(644, 'DISTRACCION', 18),
(645, 'EL MOLINO', 18),
(646, 'FONSECA', 18),
(647, 'HATONUEVO', 18),
(648, 'LA JAGUA DEL PILAR', 18),
(649, 'MAICAO', 18),
(650, 'MANAURE', 18),
(651, 'SAN JUAN DEL CESAR', 18),
(652, 'URIBIA', 18),
(653, 'URUMITA', 18),
(654, 'VILLANUEVA', 18),
(655, 'SANTA MARTA', 19),
(656, 'ALGARROBO', 19),
(657, 'ARACATACA', 19),
(658, 'ARIGUANI', 19),
(659, 'CERRO SAN ANTONIO', 19),
(660, 'CHIBOLO', 19),
(661, 'CIENAGA', 19),
(662, 'CONCORDIA', 19),
(663, 'EL BANCO', 19),
(664, 'EL PIÑON', 19),
(665, 'EL RETEN', 19),
(666, 'FUNDACION', 19),
(667, 'GUAMAL', 19),
(668, 'NUEVA GRANADA', 19),
(669, 'PEDRAZA', 19),
(670, 'PIJIÑO DEL CARMEN', 19),
(671, 'PIVIJAY', 19),
(672, 'PLATO', 19),
(673, 'PUEBLOVIEJO', 19),
(674, 'REMOLINO', 19),
(675, 'SABANAS DE SAN ANGEL', 19),
(676, 'SALAMINA', 19),
(677, 'SAN SEBASTIAN DE BUENAVISTA', 19),
(678, 'SAN ZENON', 19),
(679, 'SANTA ANA', 19),
(680, 'SANTA BARBARA DE PINTO', 19),
(681, 'SITIONUEVO', 19),
(682, 'TENERIFE', 19),
(683, 'ZAPAYAN', 19),
(684, 'ZONA BANANERA', 19),
(685, 'VILLAVICENCIO', 20),
(686, 'ACACIAS', 20),
(687, 'BARRANCA DE UPIA', 20),
(688, 'CABUYARO', 20),
(689, 'CASTILLA LA NUEVA', 20),
(690, 'CUBARRAL', 20),
(691, 'CUMARAL', 20),
(692, 'EL CALVARIO', 20),
(693, 'EL CASTILLO', 20),
(694, 'EL DORADO', 20),
(695, 'FUENTE DE ORO', 20),
(696, 'GRANADA', 20),
(697, 'GUAMAL', 20),
(698, 'MAPIRIPAN', 20),
(699, 'MESETAS', 20),
(700, 'LA MACARENA', 20),
(701, 'URIBE', 20),
(702, 'LEJANIAS', 20),
(703, 'PUERTO CONCORDIA', 20),
(704, 'PUERTO GAITAN', 20),
(705, 'PUERTO LOPEZ', 20),
(706, 'PUERTO LLERAS', 20),
(707, 'PUERTO RICO', 20),
(708, 'RESTREPO', 20),
(709, 'SAN CARLOS DE GUAROA', 20),
(710, 'SAN JUAN DE ARAMA', 20),
(711, 'SAN JUANITO', 20),
(712, 'SAN MARTIN', 20),
(713, 'VISTAHERMOSA', 20),
(714, 'PASTO', 21),
(715, 'ALBAN', 21),
(716, 'ALDANA', 21),
(717, 'ANCUYA', 21),
(718, 'ARBOLEDA', 21),
(719, 'BARBACOAS', 21),
(720, 'BELEN', 21),
(721, 'BUESACO', 21),
(722, 'COLON', 21),
(723, 'CONSACA', 21),
(724, 'CONTADERO', 21),
(725, 'CORDOBA', 21),
(726, 'CUASPUD', 21),
(727, 'CUMBAL', 21),
(728, 'CUMBITARA', 21),
(729, 'CHACHAGsI', 21),
(730, 'EL CHARCO', 21),
(731, 'EL PEÑOL', 21),
(732, 'EL ROSARIO', 21),
(733, 'EL TABLON DE GOMEZ', 21),
(734, 'EL TAMBO', 21),
(735, 'FUNES', 21),
(736, 'GUACHUCAL', 21),
(737, 'GUAITARILLA', 21),
(738, 'GUALMATAN', 21),
(739, 'ILES', 21),
(740, 'IMUES', 21),
(741, 'IPIALES', 21),
(742, 'LA CRUZ', 21),
(743, 'LA FLORIDA', 21),
(744, 'LA LLANADA', 21),
(745, 'LA TOLA', 21),
(746, 'LA UNION', 21),
(747, 'LEIVA', 21),
(748, 'LINARES', 21),
(749, 'LOS ANDES', 21),
(750, 'MAGsI', 21),
(751, 'MALLAMA', 21),
(752, 'MOSQUERA', 21),
(753, 'NARIÑO', 21),
(754, 'OLAYA HERRERA', 21),
(755, 'OSPINA', 21),
(756, 'FRANCISCO PIZARRO', 21),
(757, 'POLICARPA', 21),
(758, 'POTOSI', 21),
(759, 'PROVIDENCIA', 21),
(760, 'PUERRES', 21),
(761, 'PUPIALES', 21),
(762, 'RICAURTE', 21),
(763, 'ROBERTO PAYAN', 21),
(764, 'SAMANIEGO', 21),
(765, 'SANDONA', 21),
(766, 'SAN BERNARDO', 21),
(767, 'SAN LORENZO', 21),
(768, 'SAN PABLO', 21),
(769, 'SAN PEDRO DE CARTAGO', 21),
(770, 'SANTA BARBARA', 21),
(771, 'SANTACRUZ', 21),
(772, 'SAPUYES', 21),
(773, 'TAMINANGO', 21),
(774, 'TANGUA', 21),
(775, 'SAN ANDRES DE TUMACO', 21),
(776, 'TUQUERRES', 21),
(777, 'YACUANQUER', 21),
(778, 'CUCUTA', 22),
(779, 'ABREGO', 22),
(780, 'ARBOLEDAS', 22),
(781, 'BOCHALEMA', 22),
(782, 'BUCARASICA', 22),
(783, 'CACOTA', 22),
(784, 'CACHIRA', 22),
(785, 'CHINACOTA', 22),
(786, 'CHITAGA', 22),
(787, 'CONVENCION', 22),
(788, 'CUCUTILLA', 22),
(789, 'DURANIA', 22),
(790, 'EL CARMEN', 22),
(791, 'EL TARRA', 22),
(792, 'EL ZULIA', 22),
(793, 'GRAMALOTE', 22),
(794, 'HACARI', 22),
(795, 'HERRAN', 22),
(796, 'LABATECA', 22),
(797, 'LA ESPERANZA', 22),
(798, 'LA PLAYA', 22),
(799, 'LOS PATIOS', 22),
(800, 'LOURDES', 22),
(801, 'MUTISCUA', 22),
(802, 'OCAÑA', 22),
(803, 'PAMPLONA', 22),
(804, 'PAMPLONITA', 22),
(805, 'PUERTO SANTANDER', 22),
(806, 'RAGONVALIA', 22),
(807, 'SALAZAR', 22),
(808, 'SAN CALIXTO', 22),
(809, 'SAN CAYETANO', 22),
(810, 'SANTIAGO', 22),
(811, 'SARDINATA', 22),
(812, 'SILOS', 22),
(813, 'TEORAMA', 22),
(814, 'TIBU', 22),
(815, 'TOLEDO', 22),
(816, 'VILLA CARO', 22),
(817, 'VILLA DEL ROSARIO', 22),
(818, 'ARMENIA', 24),
(819, 'BUENAVISTA', 24),
(820, 'CALARCA', 24),
(821, 'CIRCASIA', 24),
(822, 'CORDOBA', 24),
(823, 'FILANDIA', 24),
(824, 'GENOVA', 24),
(825, 'LA TEBAIDA', 24),
(826, 'MONTENEGRO', 24),
(827, 'PIJAO', 24),
(828, 'QUIMBAYA', 24),
(829, 'SALENTO', 24),
(830, 'PEREIRA', 25),
(831, 'APIA', 25),
(832, 'BALBOA', 25),
(833, 'BELEN DE UMBRIA', 25),
(834, 'DOSQUEBRADAS', 25),
(835, 'GUATICA', 25),
(836, 'LA CELIA', 25),
(837, 'LA VIRGINIA', 25),
(838, 'MARSELLA', 25),
(839, 'MISTRATO', 25),
(840, 'PUEBLO RICO', 25),
(841, 'QUINCHIA', 25),
(842, 'SANTA ROSA DE CABAL', 25),
(843, 'SANTUARIO', 25),
(844, 'BUCARAMANGA', 27),
(845, 'AGUADA', 27),
(846, 'ALBANIA', 27),
(847, 'ARATOCA', 27),
(848, 'BARBOSA', 27),
(849, 'BARICHARA', 27),
(850, 'BARRANCABERMEJA', 27),
(851, 'BETULIA', 27),
(852, 'BOLIVAR', 27),
(853, 'CABRERA', 27),
(854, 'CALIFORNIA', 27),
(855, 'CAPITANEJO', 27),
(856, 'CARCASI', 27),
(857, 'CEPITA', 27),
(858, 'CERRITO', 27),
(859, 'CHARALA', 27),
(860, 'CHARTA', 27),
(861, 'CHIMA', 27),
(862, 'CHIPATA', 27),
(863, 'CIMITARRA', 27),
(864, 'CONCEPCION', 27),
(865, 'CONFINES', 27),
(866, 'CONTRATACION', 27),
(867, 'COROMORO', 27),
(868, 'CURITI', 27),
(869, 'EL CARMEN DE CHUCURI', 27),
(870, 'EL GUACAMAYO', 27),
(871, 'EL PEÑON', 27),
(872, 'EL PLAYON', 27),
(873, 'ENCINO', 27),
(874, 'ENCISO', 27),
(875, 'FLORIAN', 27),
(876, 'FLORIDABLANCA', 27),
(877, 'GALAN', 27),
(878, 'GAMBITA', 27),
(879, 'GIRON', 27),
(880, 'GUACA', 27),
(881, 'GUADALUPE', 27),
(882, 'GUAPOTA', 27),
(883, 'GUAVATA', 27),
(884, 'GsEPSA', 27),
(885, 'HATO', 27),
(886, 'JESUS MARIA', 27),
(887, 'JORDAN', 27),
(888, 'LA BELLEZA', 27),
(889, 'LANDAZURI', 27),
(890, 'LA PAZ', 27),
(891, 'LEBRIJA', 27),
(892, 'LOS SANTOS', 27),
(893, 'MACARAVITA', 27),
(894, 'MALAGA', 27),
(895, 'MATANZA', 27),
(896, 'MOGOTES', 27),
(897, 'MOLAGAVITA', 27),
(898, 'OCAMONTE', 27),
(899, 'OIBA', 27),
(900, 'ONZAGA', 27),
(901, 'PALMAR', 27),
(902, 'PALMAS DEL SOCORRO', 27),
(903, 'PARAMO', 27),
(904, 'PIEDECUESTA', 27),
(905, 'PINCHOTE', 27),
(906, 'PUENTE NACIONAL', 27),
(907, 'PUERTO PARRA', 27),
(908, 'PUERTO WILCHES', 27),
(909, 'RIONEGRO', 27),
(910, 'SABANA DE TORRES', 27),
(911, 'SAN ANDRES', 27),
(912, 'SAN BENITO', 27),
(913, 'SAN GIL', 27),
(914, 'SAN JOAQUIN', 27),
(915, 'SAN JOSE DE MIRANDA', 27),
(916, 'SAN MIGUEL', 27),
(917, 'SAN VICENTE DE CHUCURI', 27),
(918, 'SANTA BARBARA', 27),
(919, 'SANTA HELENA DEL OPON', 27),
(920, 'SIMACOTA', 27),
(921, 'SOCORRO', 27),
(922, 'SUAITA', 27),
(923, 'SUCRE', 27),
(924, 'SURATA', 27),
(925, 'TONA', 27),
(926, 'VALLE DE SAN JOSE', 27),
(927, 'VELEZ', 27),
(928, 'VETAS', 27),
(929, 'VILLANUEVA', 27),
(930, 'ZAPATOCA', 27),
(931, 'SINCELEJO', 27),
(932, 'BUENAVISTA', 27),
(933, 'CAIMITO', 27),
(934, 'COLOSO', 27),
(935, 'COROZAL', 27),
(936, 'COVEÑAS', 27),
(937, 'CHALAN', 27),
(938, 'EL ROBLE', 27),
(939, 'GALERAS', 27),
(940, 'GUARANDA', 27),
(941, 'LA UNION', 27),
(942, 'LOS PALMITOS', 27),
(943, 'MAJAGUAL', 27),
(944, 'MORROA', 27),
(945, 'OVEJAS', 27),
(946, 'PALMITO', 27),
(947, 'SAMPUES', 27),
(948, 'SAN BENITO ABAD', 27),
(949, 'SAN JUAN DE BETULIA', 27),
(950, 'SAN MARCOS', 27),
(951, 'SAN ONOFRE', 27),
(952, 'SAN PEDRO', 27),
(953, 'SAN LUIS DE SINCE', 27),
(954, 'SUCRE', 27),
(955, 'SANTIAGO DE TOLU', 27),
(956, 'TOLU VIEJO', 27),
(957, 'IBAGUE', 29),
(958, 'ALPUJARRA', 29),
(959, 'ALVARADO', 29),
(960, 'AMBALEMA', 29),
(961, 'ANZOATEGUI', 29),
(962, 'ARMERO', 29),
(963, 'ATACO', 29),
(964, 'CAJAMARCA', 29),
(965, 'CARMEN DE APICALA', 29),
(966, 'CASABIANCA', 29),
(967, 'CHAPARRAL', 29),
(968, 'COELLO', 29),
(969, 'COYAIMA', 29),
(970, 'CUNDAY', 29),
(971, 'DOLORES', 29),
(972, 'ESPINAL', 29),
(973, 'FALAN', 29),
(974, 'FLANDES', 29),
(975, 'FRESNO', 29),
(976, 'GUAMO', 29),
(977, 'HERVEO', 29),
(978, 'HONDA', 29),
(979, 'ICONONZO', 29),
(980, 'LERIDA', 29),
(981, 'LIBANO', 29),
(982, 'MARIQUITA', 29),
(983, 'MELGAR', 29),
(984, 'MURILLO', 29),
(985, 'NATAGAIMA', 29),
(986, 'ORTEGA', 29),
(987, 'PALOCABILDO', 29),
(988, 'PIEDRAS', 29),
(989, 'PLANADAS', 29),
(990, 'PRADO', 29),
(991, 'PURIFICACION', 29),
(992, 'RIOBLANCO', 29),
(993, 'RONCESVALLES', 29),
(994, 'ROVIRA', 29),
(995, 'SALDAÑA', 29),
(996, 'SAN ANTONIO', 29),
(997, 'SAN LUIS', 29),
(998, 'SANTA ISABEL', 29),
(999, 'SUAREZ', 29),
(1000, 'VALLE DE SAN JUAN', 29),
(1001, 'VENADILLO', 29),
(1002, 'VILLAHERMOSA', 29),
(1003, 'VILLARRICA', 29),
(1004, 'CALI', 30),
(1005, 'ALCALA', 30),
(1006, 'ANDALUCIA', 30),
(1007, 'ANSERMANUEVO', 30),
(1008, 'ARGELIA', 30),
(1009, 'BOLIVAR', 30),
(1010, 'BUENAVENTURA', 30),
(1011, 'GUADALAJARA DE BUGA', 30),
(1012, 'BUGALAGRANDE', 30),
(1013, 'CAICEDONIA', 30),
(1014, 'CALIMA', 30),
(1015, 'CANDELARIA', 30),
(1016, 'CARTAGO', 30),
(1017, 'DAGUA', 30),
(1018, 'EL AGUILA', 30),
(1019, 'EL CAIRO', 30),
(1020, 'EL CERRITO', 30),
(1021, 'EL DOVIO', 30),
(1022, 'FLORIDA', 30),
(1023, 'GINEBRA', 30),
(1024, 'GUACARI', 30),
(1025, 'JAMUNDI', 30),
(1026, 'LA CUMBRE', 30),
(1027, 'LA UNION', 30),
(1028, 'LA VICTORIA', 30),
(1029, 'OBANDO', 30),
(1030, 'PALMIRA', 30),
(1031, 'PRADERA', 30),
(1032, 'RESTREPO', 30),
(1033, 'RIOFRIO', 30),
(1034, 'ROLDANILLO', 30),
(1035, 'SAN PEDRO', 30),
(1036, 'SEVILLA', 30),
(1037, 'TORO', 30),
(1038, 'TRUJILLO', 30),
(1039, 'TULUA', 30),
(1040, 'ULLOA', 30),
(1041, 'VERSALLES', 30),
(1042, 'VIJES', 30),
(1043, 'YOTOCO', 30),
(1044, 'YUMBO', 30),
(1045, 'ZARZAL', 30),
(1046, 'ARAUCA', 3),
(1047, 'ARAUQUITA', 3),
(1048, 'CRAVO NORTE', 3),
(1049, 'FORTUL', 3),
(1050, 'PUERTO RONDON', 3),
(1051, 'SARAVENA', 3),
(1052, 'TAME', 3),
(1053, 'YOPAL', 9),
(1054, 'AGUAZUL', 9),
(1055, 'CHAMEZA', 9),
(1056, 'HATO COROZAL', 9),
(1057, 'LA SALINA', 9),
(1058, 'MANI', 9),
(1059, 'MONTERREY', 9),
(1060, 'NUNCHIA', 9),
(1061, 'OROCUE', 9),
(1062, 'PAZ DE ARIPORO', 9),
(1063, 'PORE', 9),
(1064, 'RECETOR', 9),
(1065, 'SABANALARGA', 9),
(1066, 'SACAMA', 9),
(1067, 'SAN LUIS DE PALENQUE', 9),
(1068, 'TAMARA', 9),
(1069, 'TAURAMENA', 9),
(1070, 'TRINIDAD', 9),
(1071, 'VILLANUEVA', 9),
(1072, 'MOCOA', 23),
(1073, 'COLON', 23),
(1074, 'ORITO', 23),
(1075, 'PUERTO ASIS', 23),
(1076, 'PUERTO CAICEDO', 23),
(1077, 'PUERTO GUZMAN', 23),
(1078, 'LEGUIZAMO', 23),
(1079, 'SIBUNDOY', 23),
(1080, 'SAN FRANCISCO', 23),
(1081, 'SAN MIGUEL', 23),
(1082, 'SANTIAGO', 23),
(1083, 'VALLE DEL GUAMUEZ', 23),
(1084, 'VILLAGARZON', 23),
(1085, 'SAN ANDRES', 26),
(1086, 'PROVIDENCIA', 26),
(1087, 'LETICIA', 1),
(1088, 'EL ENCANTO', 1),
(1089, 'LA CHORRERA', 1),
(1090, 'LA PEDRERA', 1),
(1091, 'LA VICTORIA', 1),
(1092, 'MIRITI - PARANA', 1),
(1093, 'PUERTO ALEGRIA', 1),
(1094, 'PUERTO ARICA', 1),
(1095, 'PUERTO NARIÑO', 1),
(1096, 'PUERTO SANTANDER', 1),
(1097, 'TARAPACA', 1),
(1098, 'INIRIDA', 15),
(1099, 'BARRANCO MINAS', 15),
(1100, 'MAPIRIPANA', 15),
(1101, 'SAN FELIPE', 15),
(1102, 'PUERTO COLOMBIA', 15),
(1103, 'LA GUADALUPE', 15),
(1104, 'CACAHUAL', 15),
(1105, 'PANA PANA', 15),
(1106, 'MORICHAL', 15),
(1107, 'SAN JOSE DEL GUAVIARE', 16),
(1108, 'CALAMAR', 16),
(1109, 'EL RETORNO', 16),
(1110, 'MIRAFLORES', 16),
(1111, 'MITU', 31),
(1112, 'CARURU', 31),
(1113, 'PACOA', 31),
(1114, 'TARAIRA', 31),
(1115, 'PAPUNAUA', 31),
(1116, 'YAVARATE', 31),
(1117, 'PUERTO CARREÑO', 32),
(1118, 'LA PRIMAVERA', 32),
(1119, 'SANTA ROSALIA', 32),
(1120, 'CUMARIBO', 32);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE IF NOT EXISTS `departamento` (
  `idDepartamento` int(11) NOT NULL,
  `NombreDepartamento` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`idDepartamento`, `NombreDepartamento`) VALUES
(1, 'AMAZONAS'),
(2, 'ANTIOQUIA'),
(3, 'ARAUCA'),
(4, 'ATLÁNTICO'),
(5, 'BOLÍVAR'),
(6, 'BOYACÁ'),
(7, 'CALDAS'),
(8, 'CAQUETÁ'),
(9, 'CASANARE'),
(10, 'CAUCA'),
(11, 'CESAR'),
(12, 'CHOCÓ'),
(13, 'CÓRDOBA'),
(14, 'CUNDINAMARCA'),
(15, 'GUAINÍA'),
(16, 'GUAVIARE'),
(17, 'HUILA'),
(18, 'LA GUAJIRA'),
(19, 'MAGDALENA'),
(20, 'META'),
(21, 'NARIÑO'),
(22, 'NORTE DE SANTANDER'),
(23, 'PUTUMAYO'),
(24, 'QUINDÍO'),
(25, 'RISARALDA'),
(26, 'SAN ANDRÉS Y ROVIDENCIA'),
(27, 'SANTANDER'),
(28, 'SUCRE'),
(29, 'TOLIMA'),
(30, 'VALLE DEL CAUCA'),
(31, 'VAUPÉS'),
(32, 'VICHADA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE IF NOT EXISTS `marca` (
  `idMarca` int(11) NOT NULL,
  `NombreMarca` varchar(45) NOT NULL,
  `IdCasaAutomovil` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`idMarca`, `NombreMarca`, `IdCasaAutomovil`) VALUES
(1, 'Ford', 1),
(2, 'Mazda', 1),
(3, 'Mitsubishi', 1),
(13, 'Renault', 3),
(14, 'Suzuki', 2),
(15, 'Volvo', 2),
(16, 'Chevrolet', 2),
(17, 'Hino', 4),
(18, 'Toyota', 4),
(19, 'Non Plus', 6),
(20, 'CKD Volkswagen', 6),
(21, 'Nissan', 7),
(22, 'Agrale', 5),
(23, 'Subaru', 8),
(24, 'Daihatsu', 8),
(25, 'DFSK', 8),
(26, 'BYD', 8),
(27, 'Mack', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelo`
--

CREATE TABLE IF NOT EXISTS `modelo` (
  `idModelo` int(11) NOT NULL,
  `NombreModelo` varchar(45) NOT NULL,
  `idMarca` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modelo`
--

INSERT INTO `modelo` (`idModelo`, `NombreModelo`, `idMarca`) VALUES
(1, 'Fusion', 1),
(2, 'Fiesta ', 1),
(3, 'Focus', 1),
(4, 'Mustang', 1),
(5, 'Ecosport', 1),
(6, 'Edge', 1),
(7, 'Escape', 1),
(8, 'Explorer', 1),
(9, 'Range', 1),
(10, 'Mazda 2', 2),
(11, '3 All New', 2),
(12, 'Mazda 3', 2),
(13, 'Mazda 6', 2),
(14, 'Mazda 626', 2),
(15, 'Allegro', 2),
(16, 'Mazda 323', 2),
(17, 'CX-5', 2),
(18, 'BT-50', 2),
(19, 'Eclipse', 3),
(20, 'Lancer', 3),
(21, 'Montero', 3),
(22, 'Logan', 13),
(23, 'Sandero', 13),
(24, 'Stepway', 13),
(25, 'Clio', 13),
(26, 'Megane', 13),
(27, 'Simbol', 13),
(28, 'Twingo', 13),
(29, 'Fluence', 13),
(30, 'Keleos', 13),
(31, 'Duster', 13),
(32, 'Logan', 13),
(33, 'Alto', 14),
(34, 'APV', 14),
(35, 'Celerio', 14),
(36, 'Ciaz', 14),
(37, 'Ertiga', 14),
(38, 'Grand Vitara', 14),
(39, 'Jimny', 14),
(40, 'S-Cross', 14),
(41, 'Vitara', 14),
(42, 'Swift', 14),
(43, 'S60', 15),
(44, 'V40', 15),
(45, 'XC60', 15),
(46, 'XC90', 15),
(47, 'Aveo', 16),
(48, 'Corsa', 16),
(49, 'Optra', 16),
(50, 'Spark', 16),
(51, 'Sail', 16),
(52, 'Cruze', 16),
(53, 'Soni', 16),
(54, 'Captiva', 16),
(55, 'Tracker', 16),
(56, 'Vitara', 16),
(57, 'Carmy', 18),
(58, 'Corolla', 18),
(59, 'Fortuner', 18),
(60, '4Runner', 18),
(61, 'Burbuja', 18),
(62, 'Prado', 18),
(63, 'Land Cruiser', 18),
(64, 'Hilux', 18),
(65, 'Tiida', 21),
(66, 'March', 21),
(67, 'Sentra', 21),
(68, 'Versa', 21),
(69, 'Qashqai', 21),
(70, 'Pathfinder', 21),
(71, 'X-Trail', 21),
(72, 'Patrol', 21),
(73, 'Impreza', 23),
(74, 'Forester', 23),
(75, 'Terios', 24),
(76, 'F0', 26),
(77, 'F3', 26),
(78, 'S6', 23),
(79, 'Dutro Max', 17),
(80, 'Dutro Pro', 17),
(81, 'Drutro Pack', 17),
(82, 'Drutro Express', 17),
(83, 'Drutro City', 17),
(84, 'Druto Team', 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multa`
--

CREATE TABLE IF NOT EXISTS `multa` (
  `idMulta` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `idVehiculo` int(11) DEFAULT NULL,
  `Fecha` date NOT NULL,
  `Articulo` varchar(255) NOT NULL,
  `Costo` int(11) NOT NULL,
  `Lugar` varchar(100) NOT NULL,
  `codAgente` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `idPersona` int(11) NOT NULL,
  `NombrePersona` varchar(45) NOT NULL,
  `NIT` varchar(15) NOT NULL,
  `ApellidoPersona` varchar(45) NOT NULL,
  `DireccionPersona` varchar(45) NOT NULL,
  `IdCiudad` int(11) NOT NULL,
  `IdTipoPersona` int(11) NOT NULL,
  `codAgente` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipopersona`
--

CREATE TABLE IF NOT EXISTS `tipopersona` (
  `idTipoPersona` int(11) NOT NULL,
  `DescripcionTipoPersona` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipopersona`
--

INSERT INTO `tipopersona` (`idTipoPersona`, `DescripcionTipoPersona`) VALUES
(1, 'Agente'),
(2, 'Infractor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `idUsuario` int(11) NOT NULL,
  `username` varchar(16) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `username`, `email`, `password`, `create_time`) VALUES
(1, 'admin', 'admin@admin.io', '1', '2016-05-13 17:04:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo`
--

CREATE TABLE IF NOT EXISTS `vehiculo` (
  `idVehiculo` int(11) NOT NULL,
  `FechaMatriculaVehuculo` date NOT NULL,
  `idPersona` int(11) NOT NULL,
  `idModelo` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `casaautomovil`
--
ALTER TABLE `casaautomovil`
  ADD PRIMARY KEY (`idCasaAutomovil`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`idCiudad`),
  ADD KEY `idDepartamento` (`idDepartamento`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`idDepartamento`),
  ADD UNIQUE KEY `NombreDepartamento_UNIQUE` (`NombreDepartamento`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`idMarca`),
  ADD KEY `idCasaAutomovil_idx` (`IdCasaAutomovil`);

--
-- Indices de la tabla `modelo`
--
ALTER TABLE `modelo`
  ADD PRIMARY KEY (`idModelo`),
  ADD KEY `idMarca_idx` (`idMarca`);

--
-- Indices de la tabla `multa`
--
ALTER TABLE `multa`
  ADD PRIMARY KEY (`idMulta`),
  ADD KEY `idPersona` (`idPersona`),
  ADD KEY `idVehiculo` (`idVehiculo`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idPersona`),
  ADD UNIQUE KEY `NIT` (`NIT`),
  ADD UNIQUE KEY `codAgente` (`codAgente`),
  ADD KEY `idTipoPersona_idx` (`IdTipoPersona`),
  ADD KEY `idCiudad_idx` (`IdCiudad`);

--
-- Indices de la tabla `tipopersona`
--
ALTER TABLE `tipopersona`
  ADD PRIMARY KEY (`idTipoPersona`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- Indices de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD PRIMARY KEY (`idVehiculo`),
  ADD KEY `idModelo_idx` (`idModelo`),
  ADD KEY `idPersona_idx` (`idPersona`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `casaautomovil`
--
ALTER TABLE `casaautomovil`
  MODIFY `idCasaAutomovil` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `idCiudad` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1121;
--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `idDepartamento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `idMarca` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `modelo`
--
ALTER TABLE `modelo`
  MODIFY `idModelo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT de la tabla `multa`
--
ALTER TABLE `multa`
  MODIFY `idMulta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `tipopersona`
--
ALTER TABLE `tipopersona`
  MODIFY `idTipoPersona` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  MODIFY `idVehiculo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `idDepartamento_fpk` FOREIGN KEY (`idDepartamento`) REFERENCES `departamento` (`idDepartamento`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `marca`
--
ALTER TABLE `marca`
  ADD CONSTRAINT `idCasaAutomovil` FOREIGN KEY (`IdCasaAutomovil`) REFERENCES `casaautomovil` (`idCasaAutomovil`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `modelo`
--
ALTER TABLE `modelo`
  ADD CONSTRAINT `idMarca` FOREIGN KEY (`idMarca`) REFERENCES `marca` (`idMarca`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `multa`
--
ALTER TABLE `multa`
  ADD CONSTRAINT `multa_ibfk_1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`),
  ADD CONSTRAINT `multa_ibfk_2` FOREIGN KEY (`idVehiculo`) REFERENCES `vehiculo` (`idVehiculo`);

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `idTipoPersona` FOREIGN KEY (`IdTipoPersona`) REFERENCES `tipopersona` (`idTipoPersona`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD CONSTRAINT `idModelo` FOREIGN KEY (`idModelo`) REFERENCES `modelo` (`idModelo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idPersona` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
