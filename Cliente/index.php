<!DOCTYPE html>
  <html lang="es">
    <head>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
      <nav>
		    <div class="nav-wrapper">
		      <a href="#" class="brand-logo center">SCOM</a>
		    </div>
		  </nav>
		  <div class="container">
        <div class="row center">
          <form action="consulta.php" method="post" class="col s12">
            <div class="row center">
              <div class="input-field col s12">
                <input name="cedula" id="input_text" type="text" length="10">
                <label for="input_text">Cédula</label>
                <button class="btn waves-effect waves-light" type="submit" name="action">Consultar
              <i class="material-icons right">send</i>
            </button>
              </div>
            </div>
            </div>
          </form>
    </div>
      </div>
        	
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
    </body>
  </html>