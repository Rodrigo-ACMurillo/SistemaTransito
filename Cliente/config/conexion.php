<?php  
	//Se cargan los datos de conexión
	$config     = parse_ini_file('config.ini');
	//Se realiza la conexión a la base de datos
	$connection = mysqli_connect('localhost',$config['username'],$config['password'],$config['dbname']);
	if($connection === false) { 
	 echo 'Ha habido un error <br>'.mysqli_connect_error(); 
	} else {
	 echo 'Conectado a la base de datos';
	}
?>