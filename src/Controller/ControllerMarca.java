/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Casaautomovil;
import Model.Marca;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author rodrigo
 */
public class ControllerMarca {
    public static List<Object> listadomarcas() {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Query query = session.getNamedQuery("findAllMarcas");
        List<Object> marcas = query.list();
        /*for (Object marca : marcas) {
            System.out.println(marca.toString());
        }*/
        return marcas;
    }
    
    public static void eliminarMarca(String nombreMarca) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Marca marca = (Marca) session.createCriteria(Marca.class).add(Restrictions.
                eq("nombreMarca", nombreMarca)).uniqueResult();
            Transaction tx = session.beginTransaction();
            Query query = session.createQuery("delete Marca m where m.nombreMarca=:nombreMarca").setString("nombreMarca", nombreMarca);
            query.executeUpdate();
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Marca eliminada"
                    + " exitosamente."
                    ,"Marca eliminada", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static boolean insertarMarca(String nombreMarca, String fabricante) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Marca marca = new Marca();
        Casaautomovil casaautomovil = (Casaautomovil) session.createCriteria(Casaautomovil.class).add(Restrictions.
                eq("nombreCasaAutomovil", fabricante)).uniqueResult();
        if(!nombreMarca.trim().isEmpty()) {
            marca.setCasaautomovil(casaautomovil);
            marca.setNombreMarca(nombreMarca);
            Transaction tx = session.beginTransaction();
            session.save(marca);
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Marca insertada"
                    + " exitosamente."
                    ,"Marca insertada", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }else {
            JOptionPane.showMessageDialog(null, "El campo nombre"
                    + " no puede estar vacio."
                    ,"Error al registrar", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
