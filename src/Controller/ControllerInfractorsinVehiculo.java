/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Ciudad;
import Model.Persona;
import Model.Tipopersona;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author rodrigo
 */
public class ControllerInfractorsinVehiculo extends ControllerInfractor {
    public static boolean insertarInfractorsinVehiculo(String nit, String nombreInfractorV, String apellidoInfractorV, String direccionInfractorV, String ciudad) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Persona infractorvehiculo = new Persona();
        Tipopersona tipopersona = (Tipopersona) session.createCriteria(Tipopersona.class).add(Restrictions.
                eq("idTipoPersona", 2)).uniqueResult();
        Ciudad miciudad = (Ciudad) session.createCriteria(Ciudad.class).add(Restrictions.
                eq("nombreCiudad", ciudad)).uniqueResult();
        if(!nombreInfractorV.trim().isEmpty() && !nit.trim().isEmpty() && !direccionInfractorV.trim().isEmpty() && !apellidoInfractorV.trim().isEmpty()) {
            infractorvehiculo.setNombrePersona(nombreInfractorV);
            infractorvehiculo.setApellidoPersona(apellidoInfractorV);
            infractorvehiculo.setCiudad(miciudad);
            infractorvehiculo.setNit(nit);
            infractorvehiculo.setDireccionPersona(direccionInfractorV);
            infractorvehiculo.setTipopersona(tipopersona);
            Transaction tx = session.beginTransaction();
            session.save(infractorvehiculo);
            session.persist(infractorvehiculo);
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Infractor SIN vehiculo insertado"
                    + " exitosamente."
                    ,"Infractor insertado", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }else {
            JOptionPane.showMessageDialog(null, "Ningún campo"
                    + " puede estar vacio."
                    ,"Error al registrar", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
