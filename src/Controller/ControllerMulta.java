/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Multa;
import Model.Persona;
import Model.Vehiculo;
import java.util.Date;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author UTP
 */
public class ControllerMulta {
    public static void eliminarMulta(String lugar) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
            Transaction tx = session.beginTransaction();
            Query query = session.createQuery("delete Multa m where m.lugar=:lugar").setString("lugar", lugar);
            query.executeUpdate();
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Multa eliminada"
                    + " exitosamente."
                    ,"Multa eliminada", JOptionPane.INFORMATION_MESSAGE);
    }
    public static boolean insertarMulta(String nit, Date fechaMulta, String articulo, int costo, String lugar, String codAgente) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Persona persona = (Persona) session.createCriteria(Persona.class).add(Restrictions.
                eq("nit", nit)).uniqueResult();
        Multa multa = new Multa();
        if(fechaMulta!=null && !nit.trim().isEmpty() && persona!=null && !articulo.trim().isEmpty() && !lugar.trim().isEmpty() && !codAgente.trim().isEmpty()) {
            Transaction tx = session.beginTransaction();
            multa.setArticulo(articulo);
            multa.setCodAgente(codAgente);
            multa.setCosto(costo);
            multa.setFecha(fechaMulta);
            multa.setLugar(lugar);
            multa.setPersona(persona);
            session.save(multa);
            tx.commit();
            JOptionPane.showMessageDialog(null, "Multa registrada"
                    + " exitosamente."
                    ,"Multa insertada", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }else {
            JOptionPane.showMessageDialog(null, "Ningún campo"
                    + " puede estar vacio."
                    ,"Error al registrar", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
    public static boolean insertarMultaVehiculo(String nit, String placa, Date fechaMulta, String articulo, Integer costo, String lugar, String codAgente) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Multa multa = new Multa();
        Vehiculo vehiculo = (Vehiculo) session.createCriteria(Vehiculo.class).add(Restrictions.
                eq("placa", placa)).uniqueResult();
        Persona persona = (Persona) session.createCriteria(Persona.class).add(Restrictions.
                eq("nit", nit)).uniqueResult();
        if(fechaMulta!=null && !nit.trim().isEmpty() && !placa.trim().isEmpty() && persona!=null && !articulo.trim().isEmpty() && !lugar.trim().isEmpty() && !codAgente.trim().isEmpty() && vehiculo!=null) {
            Transaction tx = session.beginTransaction();
            multa.setArticulo(articulo);
            multa.setCodAgente(codAgente);
            multa.setCosto(costo);
            multa.setFecha(fechaMulta);
            multa.setLugar(lugar);
            multa.setPersona(persona);
            multa.setVehiculo(vehiculo);
            session.save(multa);
            tx.commit();
            JOptionPane.showMessageDialog(null, "Multa registrada"
                    + " exitosamente."
                    ,"Multa insertada", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }else {
            JOptionPane.showMessageDialog(null, "Ningún campo"
                    + " puede estar vacio."
                    ,"Error al registrar", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
