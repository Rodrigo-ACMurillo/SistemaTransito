/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Casaautomovil;
import Model.Marca;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author rodrigo
 */
public class ControllerCasaautomovil {
    public static void eliminarFabricante(String casa) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
            Transaction tx = session.beginTransaction();
            Query query = session.createQuery("delete Casaautomovil m where m.nombreCasaAutomovil=:nombreCasaAutomovil").setString("nombreCasaAutomovil", casa);
            query.executeUpdate();
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Fabricante eliminado"
                    + " exitosamente."
                    ,"Fabricante eliminado", JOptionPane.INFORMATION_MESSAGE);
    }
    public static List<Object> listadofabricantes() {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Query query = session.getNamedQuery("findAllFabricantes");
        List<Object> fabricantes = query.list();
        return fabricantes;
    }
    
    public static boolean insertarCasa(String nombreCasa, String direccion) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Casaautomovil casaautomovil = new Casaautomovil();
        if(!(nombreCasa.trim().isEmpty()) && !(direccion.trim().isEmpty())) {
            casaautomovil.setNombreCasaAutomovil(nombreCasa);
            casaautomovil.setDireccion(direccion);
            Transaction tx = session.beginTransaction();
            session.save(casaautomovil);
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Fabricante insertado"
                    + " exitosamente."
                    ,"Fabricante insertado", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }else {
            JOptionPane.showMessageDialog(null, "Los campos"
                    + " no pueden estar vacio."
                    ,"Error al registrar", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
