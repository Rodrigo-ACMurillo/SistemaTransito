/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Usuario;
import View.Infractwo;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author RodrigoPC
 */
public class ControllerUsuario {
    public static boolean login(JFrame login,String username, String password){
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Usuario busqueda = (Usuario) session.createCriteria(Usuario.class).add(Restrictions.
                eq("username", username)).uniqueResult();
        if(busqueda!=null)
        {
            //Indica que se encontro y que si es el mismo usuario con la misma contraseña
            if(busqueda.getPassword().equals(password))
            {
                Infractwo infraccion = new Infractwo();
                login.dispose();
                Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                infraccion.setLocation(dim.width/2-infraccion.getSize().width/2, dim.height/2-infraccion.getSize().height/2);
                infraccion.setResizable(false);
                infraccion.setVisible(true);
                return true;
            }
            else
            {
                JOptionPane.showMessageDialog(null, "Verique que "
                    + "que haya escrito bien su contraseña"
                    ,"Contraseña no Valida", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            //Busqueda
        }
        else
        {
            JOptionPane.showMessageDialog(null, "El usuario "+username+ " no existe"+ "\n Verique que "
                    + "que haya escrito bien el usuario"
                    ,"Usuario no encontrado", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
    public static void cambiarpass(String newpass) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        String hqlUpdate = "update Usuario set password = :newpass where idUsuario = 1";
        session.createQuery( hqlUpdate )
                .setString( "newpass", newpass )
                .executeUpdate();
        tx.commit();
        session.close();
    }
    
    public static String buscarpass() {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        String mypass;
        Usuario user = (Usuario) session.createQuery("SELECT u FROM Usuario u WHERE id=1").uniqueResult();
        mypass = user.getPassword();
        session.close();
        return mypass;
    }
}
