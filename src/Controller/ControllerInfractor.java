/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Marca;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author rodrigo
 */
public class ControllerInfractor {
    public static List<Object> listadoinfractores() {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Query query = session.getNamedQuery("findAllInfractores");
        List<Object> infractores = query.list();
        return infractores;
    }
    
    public static void eliminarInfractor(String nombreInfractor) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
            Transaction tx = session.beginTransaction();
            Query query = session.createQuery("delete Persona m where m.nombrePersona=:nombrePersona").setString("nombrePersona", nombreInfractor);
            query.executeUpdate();
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Persona eliminada"
                    + " exitosamente."
                    ,"Persona eliminada", JOptionPane.INFORMATION_MESSAGE);
    }
}
