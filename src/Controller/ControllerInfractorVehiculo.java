/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Ciudad;
import Model.Modelo;
import Model.Persona;
import Model.Tipopersona;
import Model.Vehiculo;
import java.util.Date;
import java.util.HashSet;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author rodrigo
 */
public class ControllerInfractorVehiculo extends ControllerInfractor{
    public static boolean insertarInfractorVehiculo(String nit, String nombreInfractorV, String apellidoInfractorV, String direccionInfractorV, String ciudad, Modelo modelo, Date fecha, String placa) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Persona infractorvehiculo = new Persona();
        Tipopersona tipopersona = (Tipopersona) session.createCriteria(Tipopersona.class).add(Restrictions.
                eq("idTipoPersona", 2)).uniqueResult();
        Ciudad miciudad = (Ciudad) session.createCriteria(Ciudad.class).add(Restrictions.
                eq("nombreCiudad", ciudad)).uniqueResult();
        if(!nombreInfractorV.trim().isEmpty() && !nit.trim().isEmpty() && !direccionInfractorV.trim().isEmpty() && !apellidoInfractorV.trim().isEmpty() && modelo!=null && fecha!=null) {
            infractorvehiculo.setNombrePersona(nombreInfractorV);
            infractorvehiculo.setApellidoPersona(apellidoInfractorV);
            infractorvehiculo.setCiudad(miciudad);
            infractorvehiculo.setNit(nit);
            infractorvehiculo.setDireccionPersona(direccionInfractorV);
            infractorvehiculo.setTipopersona(tipopersona);
            Transaction tx = session.beginTransaction();
            session.save(infractorvehiculo);
            session.persist(infractorvehiculo);
            Vehiculo vehiculo = new Vehiculo();
            vehiculo.setFechaMatriculaVehuculo(fecha);
            vehiculo.setModelo(modelo);
            vehiculo.setPlaca(placa);
            vehiculo.setPersona(infractorvehiculo);
            HashSet<Vehiculo> vehiculos = new HashSet<>();
            vehiculos.add(vehiculo);
            session.persist(vehiculo);
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Infractor con vehiculo insertado"
                    + " exitosamente."
                    ,"Infractor insertado", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }else {
            JOptionPane.showMessageDialog(null, "Ningún campo"
                    + " puede estar vacio."
                    ,"Error al registrar", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
