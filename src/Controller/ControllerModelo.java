/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Marca;
import Model.Modelo;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author rodrigo
 */
public class ControllerModelo {
    public static List<Object> listadomodelos() {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Query query = session.getNamedQuery("findAllModelos");
        List<Object> modelos = query.list();
        return modelos;
    }
    
    public static boolean insertarModelo(String nombreModelo, String marca) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Modelo modelo = new Modelo();
        Marca marcaselected = (Marca) session.createCriteria(Marca.class).add(Restrictions.
                eq("nombreMarca", marca)).uniqueResult();
        if(!nombreModelo.trim().isEmpty()) {
            modelo.setMarca(marcaselected);
            modelo.setNombreModelo(nombreModelo);
            Transaction tx = session.beginTransaction();
            session.save(modelo);
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Modelo insertado"
                    + " exitosamente."
                    ,"Modelo insertado", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }else {
            JOptionPane.showMessageDialog(null, "El campo nombre"
                    + " no puede estar vacio."
                    ,"Error al registrar", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
    public static void eliminarModelo(String model) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
            Transaction tx = session.beginTransaction();
            Query query = session.createQuery("delete Modelo m where m.nombreModelo=:model").setString("model", model);
            query.executeUpdate();
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Modelo eliminado"
                    + " exitosamente."
                    ,"Modelo eliminado", JOptionPane.INFORMATION_MESSAGE);
    }
}
