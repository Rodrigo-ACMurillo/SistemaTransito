/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Ciudad;
import Model.Persona;
import Model.Tipopersona;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author rodrigo
 */
public class ControllerAgente {
    public static void eliminarAgente(String cod) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
            Transaction tx = session.beginTransaction();
            Query query = session.createQuery("delete Persona m where m.codAgente=:cod").setString("cod", cod);
            query.executeUpdate();
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Agente eliminado"
                    + " exitosamente."
                    ,"agente eliminado", JOptionPane.INFORMATION_MESSAGE);
    }
    public static List<Object> listadoAgentes() {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Query query = session.getNamedQuery("findAllAgentes");
        List<Object> agentes = query.list();
        return agentes;
    }
    public static boolean insertarAgente(String nit, String nombreAgente, String apellidoAgente, String direccionAgente, Integer codAgente, String ciudad) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Persona agente = new Persona();
        Tipopersona tipopersona = (Tipopersona) session.createCriteria(Tipopersona.class).add(Restrictions.
                eq("idTipoPersona", 1)).uniqueResult();
        Ciudad miciudad = (Ciudad) session.createCriteria(Ciudad.class).add(Restrictions.
                eq("nombreCiudad", ciudad)).uniqueResult();
        if(!nombreAgente.trim().isEmpty() && !nit.trim().isEmpty() && !direccionAgente.trim().isEmpty() && !apellidoAgente.trim().isEmpty() && codAgente!=null) {
            agente.setNombrePersona(nombreAgente);
            agente.setApellidoPersona(apellidoAgente);
            agente.setCiudad(miciudad);
            agente.setNit(nit);
            agente.setDireccionPersona(direccionAgente);
            agente.setCodAgente(codAgente);
            agente.setTipopersona(tipopersona);
            Transaction tx = session.beginTransaction();
            session.save(agente);
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Agente insertado"
                    + " exitosamente."
                    ,"Agente insertado", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }else {
            JOptionPane.showMessageDialog(null, "Ningún campo"
                    + " puede estar vacio."
                    ,"Error al registrar", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
