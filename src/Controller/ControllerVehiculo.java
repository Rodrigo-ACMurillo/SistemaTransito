/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Modelo;
import Model.Persona;
import Model.Vehiculo;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author rodrigo
 */
public class ControllerVehiculo {
    public static void eliminarVehiculo(String placa) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
            Transaction tx = session.beginTransaction();
            Query query = session.createQuery("delete Vehiculo m where m.placa=:placa").setString("placa", placa);
            query.executeUpdate();
            session.close();
            tx.commit();
            JOptionPane.showMessageDialog(null, "Vehiculo eliminado"
                    + " exitosamente."
                    ,"Vehiculo eliminado", JOptionPane.INFORMATION_MESSAGE);
    }
    public static List<Object> listadovehiculosperson(String nit) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Query query = session.getNamedQuery("findVehiculos").setString("nit", nit);
        List<Object> vehiculos = query.list();
        return vehiculos;
    }
    public static boolean insertarVehiculo(Date fechaMatricula, Modelo modelo, String placa, String nit) {
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        Vehiculo vehiculo = new Vehiculo();
        Persona persona = (Persona) session.createCriteria(Persona.class).add(Restrictions.
                eq("nit", nit)).uniqueResult();
        if(fechaMatricula!=null && !nit.trim().isEmpty() && modelo!=null) {
            vehiculo.setFechaMatriculaVehuculo(fechaMatricula);
            vehiculo.setModelo(modelo);
            vehiculo.setPlaca(placa);
            vehiculo.setPersona(persona);
            Transaction tx = session.beginTransaction();
            session.save(vehiculo);
            session.persist(vehiculo);
            HashSet<Vehiculo> vehiculos = new HashSet<>();
            vehiculos.add(vehiculo);
            session.persist(vehiculo);
            tx.commit();
            JOptionPane.showMessageDialog(null, "Vehiculo insertado"
                    + " exitosamente."
                    ,"Vehiculo insertado", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }else {
            JOptionPane.showMessageDialog(null, "Ningún campo"
                    + " puede estar vacio."
                    ,"Error al registrar", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
